/** 
 * @file        aws.c
 * @brief       This file contains functions for publish and subscribe 
 *              to AWS IoT Core. 
 *
 */
/* TODO: Error Handling for the library
/*
 *#####################################################################
 *  Initialization block
 *  ---------------------
 *#####################################################################
 */

/* --- Standard Includes --- */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <pthread.h>

/* --- Project Includes --- */
#include <aws.h>
#include <awsiotsdk/aws_iot_config.h>
#include <awsiotsdk/aws_iot_log.h>
#include <awsiotsdk/aws_iot_version.h>
#include <awsiotsdk/aws_iot_mqtt_client_interface.h>

#define TOPIC "gui/test"

#define       PING              1
#define       SET_BPM           2
#define       SET_VOLUME        3
#define       SET_IE            4
#define       READ_DEVID        5
#define       READ_HG           6
#define       READ_IE           7
#define       READ_VOLUME       8
#define       READ_PEEP         9


struct AWSSettings set = {

        /* MQTT HOST URL */
        .hostURL="a3rh361ag55y5n-ats.iot.us-east-2.amazonaws.com",
        /* MQTT port */
        .port=8883,
        /* Cert location */
        .certDir="../certs/",
        /* Client Root CA file name */
        .rootCA="AmazonRootCA1.pem",
        /* Client certificate file name */
        .certName="187a480729-certificate.pem.crt",
        /* Client Private Key file name */
        .privKey="187a480729-private.pem.key",
        /* Quality Of Service 0 or 1 */
        .QOS=1,
        /* Client ID */
        .clientID="test-3-25"
};

AWS_IoT_Client *pSubClient;

/* Global variable for storing interrupt function */
static void (*subscribe_callback)(int topicNameLen, char *topicName, int msgLen, char *msg);

static void sub_callback (AWS_IoT_Client *pClient, char *topicName, uint16_t topicNameLen, IoT_Publish_Message_Params *params, void *pData) 
{
	IOT_UNUSED(pData);
	IOT_UNUSED(pClient);
	subscribe_callback(topicNameLen, topicName, (int) params->payloadLen, (char *) params->payload);
	//printf("Subscribe callback\n");
	//printf("%.*s\t%.*s\n", topicNameLen, topicName, (int) params->payloadLen, (char *) params->payload);
}

void remoteControl(int topicNameLen, char *topicName, int msgLen, char *msg) 
{

        int cmd = 0;

        if(strncmp(msg, "set bpm", 7) == 0 ) {
                cmd = SET_BPM;
        } else if(strncmp(msg, "set volume", 10) == 0 ) {
                cmd = SET_VOLUME;  
        } else if(strncmp(msg, "set ie", 6) == 0 ) {
                cmd = SET_VOLUME;  
        } else if(strncmp(msg, "read devid", 10) == 0 ) {
                cmd = READ_DEVID;  
        } else if(strncmp(msg, "read hg", 7) == 0 ) {
                cmd = READ_HG;  
        } else if(strncmp(msg, "read ie", 7) == 0 ) {
                cmd = READ_IE;  
        } else if(strncmp(msg, "read volume", 11) == 0 ) {
                cmd = READ_VOLUME;  
        } else if(strncmp(msg, "read peep", 9) == 0 ) {
                cmd = READ_PEEP;  
        } else if(strncmp(msg, "ping", 4) == 0 ) {
                cmd = PING;
        }

        switch (cmd) {
                case PING:
                        printf("Ping by AWS remote.\n");
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), "Ping received by device, Ping Back.", strlen("Ping received by device, Ping Back."));  
                        break;
                case SET_BPM:
                        printf("Triggered BPM setting by AWS remote.\n");
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), "Incorrect BPM value, Unable to set BPM.", strlen("Incorrect BPM value, Unable to set BPM.")); 
                        break;
                case SET_VOLUME:                   
                        printf("Triggered Volume setting by AWS remote.\n");
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), "Incorrect Volume, Unable to set Volume.", strlen("Incorrect Volume, Unable to set Volume."));  
                        break;
                case SET_IE: 
                        printf("Triggered IE setting by AWS remote.\n");
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), "Incorrect IE, Unable to set IE.", strlen("Incorrect IE, Unable to set IE."));  
                        break;
                case READ_DEVID: 
                        printf("Triggered Read ID setting by AWS remote.\n");
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), "{ \"id\": device}", strlen("{ \"id\": device}"));  
                        break;
                case READ_IE: 
                        printf("Triggered Read ID setting by AWS remote.\n");
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), "{ \"ie\": \"1/4\"}", strlen("{ \"id\": device}"));  
                        break;
                case READ_HG: 
                        printf("Triggered Read ID setting by AWS remote.\n");
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), "{ \"pressure\": \"40\"}", strlen("{ \"id\": device}"));  
                        break;
                case READ_VOLUME: 
                        printf("Triggered Read ID setting by AWS remote.\n");
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), "{ \"volume\": 80}", strlen("{ \"id\": device}"));  
                        break;
                case READ_PEEP: 
                        printf("Triggered Read ID setting by AWS remote.\n");
                        aws_iot_pub (set, TOPIC, strlen(TOPIC), "{ \"peep\": 63}", strlen("{ \"id\": device}"));  
                        break;
                default : return ;
        }

}


void aws_loop ()
{
        IoT_Error_t rc = FAILURE;

        //Max time the yield function will wait for read messages
        rc = aws_iot_mqtt_yield(pSubClient, MQTT_MAX_YEILD_TIME);
}

static void disconn_handler(AWS_IoT_Client *pClient, void *data) 
{
	IOT_WARN("MQTT Disconnect");
	IoT_Error_t rc = FAILURE;

	if(NULL == pClient) {
		return;
	}

	IOT_UNUSED(data);

	if(aws_iot_is_autoreconnect_enabled(pClient)) {
		//IOT_INFO("Auto Reconnect is enabled, Reconnecting attempt will start now");
	} else {
		IOT_WARN("Auto Reconnect not enabled. Starting manual reconnect...");
		rc = aws_iot_mqtt_attempt_reconnect(pClient);
		if(NETWORK_RECONNECTED == rc) {
			IOT_WARN("Manual Reconnect Successful");
		} else {
			IOT_WARN("Manual Reconnect Failed - %d", rc);
		}
	}
}

IoT_Error_t aws_iot_pub (struct AWSSettings set, char *topic, int32_t topicLen, char *msg, int32_t msgLen) 
{
	IoT_Error_t rc = FAILURE;

	AWS_IoT_Client client;
	IoT_Client_Init_Params init_args = iotClientInitParamsDefault;
	IoT_Client_Connect_Params connect_args = iotClientConnectParamsDefault;

	IoT_Publish_Message_Params msg_params;
	
	subscribe_callback = remoteControl;	
	
	char rootCA[PATH_MAX + 1];
	char clientCRT[PATH_MAX + 1];
	char clientKey[PATH_MAX + 1];
	char CurrentWD[PATH_MAX + 1];

	getcwd(CurrentWD, sizeof(CurrentWD));
        snprintf(rootCA, PATH_MAX + 1, "%s/%s/%s", CurrentWD, set.certDir, set.rootCA);
	snprintf(clientCRT, PATH_MAX + 1, "%s/%s/%s", CurrentWD, set.certDir, set.certName);
	snprintf(clientKey, PATH_MAX + 1, "%s/%s/%s", CurrentWD, set.certDir, set.privKey);

	//IOT_DEBUG("rootCA %s", rootCA);
	//IOT_DEBUG("clientCRT %s", clientCRT);
	//IOT_DEBUG("clientKey %s", clientKey);

        /* Set Initialization Arguments */
	init_args.enableAutoReconnect = false;
	init_args.pHostURL = set.hostURL;
	init_args.port = set.port;
	init_args.pRootCALocation = rootCA;
	init_args.pDeviceCertLocation = clientCRT;
	init_args.pDevicePrivateKeyLocation = clientKey;
	init_args.mqttCommandTimeout_ms = MQTT_COMMAND_TIMEOUT_MS;
	init_args.tlsHandshakeTimeout_ms = TLS_HANDSHAKE_TIMEOUT_MS;
	init_args.isSSLHostnameVerify = true;
	init_args.disconnectHandler = disconn_handler;
	init_args.disconnectHandlerData = NULL;

	rc = aws_iot_mqtt_init(&client, &init_args);
	
        if(SUCCESS != rc) {
		IOT_ERROR("aws_iot_mqtt_init returned error : %d ", rc);
		return rc;
	}
	/**
	 * #####################################################################
	 *  Connect
	 * #####################################################################
	 * 
	 */
	connect_args.keepAliveIntervalInSec = KEEP_ALIVE_INTERVAL_IN_SEC;
	connect_args.isCleanSession = true;
	connect_args.MQTTVersion = MQTT_3_1_1;
	connect_args.pClientID = set.hostURL;
	connect_args.clientIDLen = (uint16_t) strlen(set.hostURL);
	connect_args.isWillMsgPresent = false;

	//IOT_INFO("Connecting...");
	rc = aws_iot_mqtt_connect(&client, &connect_args);
	if(SUCCESS != rc) {
		IOT_ERROR("Error(%d) connecting to %s:%d", rc, init_args.pHostURL, init_args.port);
		return rc;
	}
	/*
	 * Enable Auto Reconnect functionality. 
         */
	rc = aws_iot_mqtt_autoreconnect_set_status(&client, true);
	if(SUCCESS != rc) {
		IOT_ERROR("Unable to set Auto Reconnect to true - %d", rc);
		return rc;
	}
	/**
	 * #####################################################################
	 *  Subscribe 
	 * #####################################################################
	 * 
	 */
	//IOT_INFO("Subscribing...");
	rc = aws_iot_mqtt_subscribe(&client, topic, topicLen, QOS0, sub_callback, NULL);
	if(SUCCESS != rc) {
		IOT_ERROR("Error subscribing : %d ", rc);
		return rc;
	}

	if (AWS_MQTT_QOS0 == set.QOS) {
		msg_params.qos = QOS0;	
	} else if (AWS_MQTT_QOS1 == set.QOS) {
		msg_params.qos = QOS1;
	} else {
		msg_params.qos = QOS0;
	}

	msg_params.payload = (void *) msg;
	msg_params.payloadLen = msgLen;
	msg_params.isRetained = 0;

        rc = aws_iot_mqtt_publish(&client, topic, topicLen, &msg_params);
        if (rc == MQTT_REQUEST_TIMEOUT_ERROR) {
                IOT_WARN("QOS1 publish ack not received.\n");
                rc = SUCCESS;
        }

	if(SUCCESS != rc) {
		IOT_ERROR("An error occurred in the loop.\n");
	} else {
		//IOT_INFO("Publish done\n");
	}
	pSubClient = &client;
	return rc;
}

IoT_Error_t aws_iot_sub (struct AWSSettings set, char *topic, int32_t topicLen, void (*callback)(int , char *, int , char *)) 
{
	IoT_Error_t rc = FAILURE;
	
	subscribe_callback = remoteControl;

	AWS_IoT_Client client;
	IoT_Client_Init_Params init_args = iotClientInitParamsDefault;
	IoT_Client_Connect_Params connect_args = iotClientConnectParamsDefault;

	IoT_Publish_Message_Params msg_params;

	char rootCA[PATH_MAX + 1];
	char clientCRT[PATH_MAX + 1];
	char clientKey[PATH_MAX + 1];
	char CurrentWD[PATH_MAX + 1];

	getcwd(CurrentWD, sizeof(CurrentWD));
        snprintf(rootCA, PATH_MAX + 1, "%s/%s/%s", CurrentWD, set.certDir, set.rootCA);
	snprintf(clientCRT, PATH_MAX + 1, "%s/%s/%s", CurrentWD, set.certDir, set.certName);
	snprintf(clientKey, PATH_MAX + 1, "%s/%s/%s", CurrentWD, set.certDir, set.privKey);

	printf("rootCA %s", rootCA);
	printf("clientCRT %s", clientCRT);
	printf("clientKey %s", clientKey);
	//IOT_DEBUG("rootCA %s", rootCA);
	//IOT_DEBUG("clientCRT %s", clientCRT);
	//IOT_DEBUG("clientKey %s", clientKey);

        /* Set Initialization Arguments */
	init_args.enableAutoReconnect = false;
	init_args.pHostURL = set.hostURL;
	init_args.port = set.port;
	init_args.pRootCALocation = rootCA;
	init_args.pDeviceCertLocation = clientCRT;
	init_args.pDevicePrivateKeyLocation = clientKey;
	init_args.mqttCommandTimeout_ms = MQTT_COMMAND_TIMEOUT_MS;
	init_args.tlsHandshakeTimeout_ms = TLS_HANDSHAKE_TIMEOUT_MS;
	init_args.isSSLHostnameVerify = true;
	init_args.disconnectHandler = disconn_handler;
	init_args.disconnectHandlerData = NULL;

	rc = aws_iot_mqtt_init(&client, &init_args);
	
        if(SUCCESS != rc) {
		IOT_ERROR("aws_iot_mqtt_init returned error : %d ", rc);
		return rc;
	}
	/**
	 * #####################################################################
	 *  Connect
	 * #####################################################################
	 * 
	 */
	connect_args.keepAliveIntervalInSec = KEEP_ALIVE_INTERVAL_IN_SEC;
	connect_args.isCleanSession = true;
	connect_args.MQTTVersion = MQTT_3_1_1;
	connect_args.pClientID = set.hostURL;
	connect_args.clientIDLen = (uint16_t) strlen(AWS_IOT_MQTT_CLIENT_ID);
	connect_args.isWillMsgPresent = false;

	//IOT_INFO("Connecting...");
	rc = aws_iot_mqtt_connect(&client, &connect_args);
	if(SUCCESS != rc) {
		IOT_ERROR("Error(%d) connecting to %s:%d", rc, init_args.pHostURL, init_args.port);
		return rc;
	}
	/*
	 * Enable Auto Reconnect functionality. 
         */
	rc = aws_iot_mqtt_autoreconnect_set_status(&client, true);
	if(SUCCESS != rc) {
		IOT_ERROR("Unable to set Auto Reconnect to true - %d", rc);
		return rc;
	}
	/**
	 * #####################################################################
	 *  Subscribe 
	 * #####################################################################
	 * 
	 */
	//IOT_INFO("Subscribing...");
	rc = aws_iot_mqtt_subscribe(&client, topic, topicLen, QOS1, sub_callback, NULL);
	if(SUCCESS != rc) {
		IOT_ERROR("Error subscribing : %d ", rc);
		return rc;
	}
	pSubClient = &client;

	if(SUCCESS != rc) {
		IOT_ERROR("An error occurred in the loop.\n");
	} else {
		//IOT_INFO("Publish done\n");
	}
	
	return rc;
}

void awsControlInit(void)
{
        aws_iot_sub (set, TOPIC, strlen(TOPIC), remoteControl);
}


void loop(void) 
{

        aws_loop();

}

void setup(void) 
{
        awsControlInit();
}

int main (void) 
{
        setup();
        while(1) {
                loop();
        }
        return 0;
}
