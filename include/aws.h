/**
 * @file aws.h
 * @brief Header file contains prototype functions for AWS IoT 
 *        implementation in Shunya interfaces. 
 */

/*! Include Guard */
#ifndef __AWS_SHUNYA_H__
#define __AWS_SHUNYA_H__

/*! CPP guard */
#ifdef __cplusplus
extern "C" {
#endif

#define HOST_ADDRESS_SIZE 255
#define CLIENT_ID_SIZE 255

#define MQTT_COMMAND_TIMEOUT_MS 20000
#define TLS_HANDSHAKE_TIMEOUT_MS 5000
#define MQTT_MAX_YEILD_TIME 100

#define KEEP_ALIVE_INTERVAL_IN_SEC 600

#define AWS_MQTT_QOS0 0
#define AWS_MQTT_QOS1 1


/** 
 * @file        aws.c
 * @brief       This file contains functions for publish and subscribe 
 *              to AWS IoT Core. 
 *
 */
/* TODO: Error Handling for the library
/*
 *#####################################################################
 *  Initialization block
 *  ---------------------
 *#####################################################################
 */

/* --- Project Includes --- */
#include <awsiotsdk/aws_iot_config.h>
#include <awsiotsdk/aws_iot_log.h>
#include <awsiotsdk/aws_iot_version.h>
#include <awsiotsdk/aws_iot_mqtt_client_interface.h>


struct AWSSettings {

        /* MQTT HOST URL */
        char hostURL[HOST_ADDRESS_SIZE];
        /* MQTT port */
        uint32_t port;
        /* Cert location */
        char certDir[PATH_MAX + 1];
        /* Client Root CA file name */
        char rootCA[PATH_MAX + 1];
        /* Client certificate file name */
        char certName[PATH_MAX + 1];
        /* Client Private Key file name */
        char privKey[PATH_MAX + 1];
        /* Quality Of Service 0 or 1 */
        uint8_t QOS;
        /* Client ID */
        char clientID [255];
};

extern IoT_Error_t aws_iot_pub (struct AWSSettings set, char *topic, int32_t topicLen, char *msg, int32_t msgLen);

extern IoT_Error_t aws_iot_sub (struct AWSSettings set, char *topic, int32_t topicLen, void (*callback)(int , char *, int , char *));


#ifdef __cplusplus
}
#endif /* End of CPP guard */

#endif /* __AWS_SHUNYA_H__ */  /* End of Include Guard */
